package com.dxapps.plankmusictimer.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Debug;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.dxapps.plankmusictimer.BuildConfig;
import com.dxapps.plankmusictimer.R;
import com.dxapps.plankmusictimer.fragments.AccountFragment;
import com.dxapps.plankmusictimer.fragments.EditExerciseFragment;
import com.dxapps.plankmusictimer.fragments.OverviewFragment;
import com.dxapps.plankmusictimer.utils.PreferenceUtils;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.appcompat.app.ActionBar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity {

    @BindView(R.id.adView) AdView mAdBar;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        if (PreferenceUtils.getInstance().isRegistered()) {
            if (getIntent().getStringExtra(Intent.EXTRA_TEXT) != null) {
                goToFragment(OverviewFragment.newInstance(getIntent().getStringExtra(Intent.EXTRA_TEXT)), false);
            } else {
                goToFragment(new OverviewFragment(), false);
            }
        } else {
            adVisibility(false);
            if (PreferenceUtils.getInstance().isFirstRun()) {
                goToFragment(AccountFragment.newInstance(true, false, false), false);
            } else {
                goToFragment(AccountFragment.newInstance(false, true, false), false);
            }
        }

        //MobileAds.initialize(this,"ca-app-pub-3940256099942544~3347511713");
        MobileAds.initialize(this, "ca-app-pub-6107094218736438~3346340183");
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdBar.loadAd(adRequest);
    }

    /**
     * Go to a {@link Fragment}. Add add it optionally to the back stack.
     * If addToBackStack is false, the stack will be cleared.
     * The back icon in the toolbar will be updated automatically.
     * Use a getInstance method on the fragment to add arguments/bundle to the fragment.
     *
     * @param fragment       Fragment to go to
     * @param addToBackStack Add to back stack
     */
    public void goToFragment(Fragment fragment, boolean addToBackStack) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        // Hide Keyboard
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        if (fragment instanceof OverviewFragment) {
            transaction.replace(R.id.container, fragment);

            if (addToBackStack) {
                transaction.addToBackStack(null);
            } else {
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }
            }
        } else {
            transaction.setCustomAnimations(R.anim.fragment_appear_from_right, R.anim.fragment_leave_left, R.anim.fragment_appear_from_left, R.anim.fragment_leave_right);
            if (addToBackStack) {
                transaction.addToBackStack(null);
            } else {
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }
            }

            transaction.replace(R.id.container, fragment);
        }
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        Log.d("Activity", "back pressed");
        FragmentManager fragmentManager = getSupportFragmentManager();
        int count = fragmentManager.getBackStackEntryCount();
        if (count == 0) {
            super.onBackPressed();
        } else if (count > 0) {
            Fragment fragment = fragmentManager.findFragmentById(R.id.container);
            if (fragment instanceof EditExerciseFragment) {
                if (((EditExerciseFragment) fragment).hasChanges()) {
                    ((EditExerciseFragment) fragment).warningDialog();
                } else {
                    fragmentManager.popBackStack();
                }
            } else {
                fragmentManager.popBackStack();
            }
        }
    }

    public void adVisibility(Boolean visible) {
        if (visible) {
            mAdBar.setVisibility(View.VISIBLE);
        } else {
            mAdBar.setVisibility(View.GONE);
        }
    }

    /**
     * method is used to extract a youtube url
     *
     * @param ytUrl full youtube url
     * @return extracted YouTube ID
     */
    public String extractYTId(String ytUrl) {
        String vId = null;
        Pattern pattern = Pattern.compile(
                "^https?://.*(?:youtu.be/|v/|u/\\w/|embed/|watch?v=)([^#&?]*).*$",
                Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(ytUrl);
        if (matcher.matches()) {
            vId = matcher.group(1);
        }
        return vId;
    }
}
