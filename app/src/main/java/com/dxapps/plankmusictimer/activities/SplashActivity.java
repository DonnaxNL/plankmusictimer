package com.dxapps.plankmusictimer.activities;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;

import androidx.fragment.app.FragmentActivity;

import com.dxapps.plankmusictimer.R;
import com.dxapps.plankmusictimer.layouts.StandardDialog;

public class SplashActivity extends FragmentActivity {

    // The amount of milliseconds that the splash screen will be active.
    private static final int DURATION = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();

        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(() -> {
            if (isNetworkAvailable()) {
                startMainActivity();
            } else {
                StandardDialog mDialog = StandardDialog.getInstance(getString(R.string.no_internet_title),
                        getString(R.string.no_internet_text), getString(R.string.ok));
                mDialog.setDialogClickListener(new StandardDialog.OnDialogClickListener() {
                    @Override
                    public void onCancelClick(StandardDialog dialog) {
                        dialog.dismiss();
                    }

                    @Override
                    public void onSubmitClick(StandardDialog dialog, String email) {

                    }
                });
                mDialog.setCancelable(false);
                mDialog.show(getSupportFragmentManager(), null);
            }
        }, DURATION);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void startMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}