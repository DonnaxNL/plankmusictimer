package com.dxapps.plankmusictimer.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dxapps.plankmusictimer.R;
import com.dxapps.plankmusictimer.models.Exercise;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ExerciseAdapter extends RecyclerView.Adapter<ExerciseAdapter.ViewHolder> {

    private Context mContext;
    private List<Exercise> exerciseList;
    private ExerciseClickListener mListener;

    public ExerciseAdapter(Context context, List<Exercise> exerciseList) {
        this.mContext = context;
        this.exerciseList = exerciseList;
    }

    public void setListener(ExerciseClickListener mListener) {
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public ExerciseAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.exercise_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Picasso.get()
                .load(mContext.getString(R.string.youtube_thumbnail, exerciseList.get(position).getVideoId()))
                .fit()
                .placeholder(R.drawable.default_thumb)
                .error(R.drawable.default_thumb)
                .into(holder.thumbnail);
        holder.name.setText(exerciseList.get(position).getName());
        holder.start.setText(mContext.getString(R.string.start_time, exerciseList.get(position).getStartTimeString()));
        holder.duration.setText(mContext.getString(R.string.timer_duration, exerciseList.get(position).getDurationString()));
        editClick(holder.edit, position, exerciseList.get(position));
        playClick(holder.play, position, exerciseList.get(position));
    }

    private void editClick(final View itemView, final int position, final Exercise selectedItem) {
        itemView.setOnClickListener(v -> {
            if (mListener != null) {
                mListener.onEditExerciseItemClick(itemView, position, selectedItem);
            }
        });
    }

    private void playClick(final View itemView, final int position, final Exercise selectedItem) {
        itemView.setOnClickListener(v -> {
            if (mListener != null) {
                mListener.onExerciseItemClick(itemView, position, selectedItem);
            }
        });
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getItemCount() {
        return exerciseList.size();
    }

    public interface ExerciseClickListener {
        void onEditExerciseItemClick(View itemView, int position, Exercise selectedItem);
        void onExerciseItemClick(View itemView, int position, Exercise selectedItem);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.exc_item_thumb) ImageView thumbnail;
        @BindView(R.id.exc_item_name) TextView name;
        @BindView(R.id.exc_item_start) TextView start;
        @BindView(R.id.exc_item_duration) TextView duration;
        @BindView(R.id.exc_item_edit) ImageView edit;
        @BindView(R.id.exc_item_play) ImageView play;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}