package com.dxapps.plankmusictimer.constants;

public class BundleKeys {
    public static final String SHARED_VIDEO_ID = "shared_video_id";
    public static final String ITEM = "item";
    public static final String VIDEO_ID = "video_id";
    public static final String TIMER = "timer";
    public static final String START_TIME = "shared_video_id";
    public static final String WEB_TITLE = "web_title";
    public static final String WEB_URL = "web_url";
    public static final String INTRO = "intro";
    public static final String LOGIN = "login";
    public static final String LINK_ACCOUNT = "link_account";
}
