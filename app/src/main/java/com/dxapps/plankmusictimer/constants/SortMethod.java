package com.dxapps.plankmusictimer.constants;

public class SortMethod {
    public static final String SORT_DIRECTION_DESC = "descending";
    public static final String SORT_DIRECTION_ASC = "ascending";
    public static final String SORT_LAST_USED = "last_used";
    public static final String SORT_CREATED = "created";
    public static final String SORT_NAME = "name";
}
