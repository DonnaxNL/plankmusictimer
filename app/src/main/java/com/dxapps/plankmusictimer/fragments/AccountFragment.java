package com.dxapps.plankmusictimer.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.crashlytics.android.Crashlytics;
import com.dxapps.plankmusictimer.R;
import com.dxapps.plankmusictimer.activities.MainActivity;
import com.dxapps.plankmusictimer.constants.BundleKeys;
import com.dxapps.plankmusictimer.layouts.StandardDialog;
import com.dxapps.plankmusictimer.utils.PreferenceUtils;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import java.util.Objects;
import java.util.concurrent.Executor;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AccountFragment extends BaseFragment implements TextWatcher {

    private final static String TAG = TimerFragment.class.getName();

    private boolean isIntroductional = true;
    private boolean isLogin = false;
    private boolean isLinkAccount = false;
    private int RC_SIGN_IN = 1000;
    private int RC_LINK = 2000;
    private FirebaseAuth mAuth;
    private GoogleSignInClient mGoogleSignInClient;

    @BindView(R.id.txt_title) TextView mTitle;
    @BindView(R.id.btn_back) ImageButton mBackButton;
    @BindView(R.id.introductional) TextView mIntroductional;
    @BindView(R.id.sign_up_layout) LinearLayout mSignUpLayout;
    @BindView(R.id.sign_up_email_layout) TextInputLayout mSignUpEmailLayout;
    @BindView(R.id.sign_up_email) EditText mSignUpEmailField;
    @BindView(R.id.sign_up_password) EditText mSignUpPasswordField;
    @BindView(R.id.agreement) CheckBox mAgreement;
    @BindView(R.id.log_in_layout) LinearLayout mLogInLayout;
    @BindView(R.id.login_email_layout) TextInputLayout mLoginEmailLayout;
    @BindView(R.id.login_email) EditText mLoginEmailField;
    @BindView(R.id.login_password_layout) TextInputLayout mLoginPasswordLayout;
    @BindView(R.id.login_password) EditText mLoginPasswordField;
    @BindView(R.id.login) TextView mLoginInstead;
    @BindView(R.id.no_account) TextView mNoAccount;
    @BindView(R.id.btn_log_in) Button mLoginButton;
    @BindView(R.id.btn_sign_up) Button mSignUpButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static AccountFragment newInstance(boolean isIntro, boolean isLogin, boolean isLinkAccount) {
        AccountFragment fragment = new AccountFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(BundleKeys.INTRO, isIntro);
        bundle.putBoolean(BundleKeys.LOGIN, isLogin);
        bundle.putBoolean(BundleKeys.LINK_ACCOUNT, isLinkAccount);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ViewGroup view = (ViewGroup) inflater.inflate(R.layout.fragment_account, container, false);
        ButterKnife.bind(this, view);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            isIntroductional = bundle.getBoolean(BundleKeys.INTRO);
            isLogin = bundle.getBoolean(BundleKeys.LOGIN);
            isLinkAccount = bundle.getBoolean(BundleKeys.LINK_ACCOUNT);
        }

        configAuth();
        setUpViews();

        return view;
    }

    private void configAuth() {
        mAuth = FirebaseAuth.getInstance();

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getContext().getString(R.string.web_client_id))
                .requestEmail()
                .build();
        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(getActivity(), gso);
    }

    private void setUpViews() {
        if (isLinkAccount) {
            mBackButton.setVisibility(View.VISIBLE);
            mTitle.setText(getString(R.string.link_account));
            mIntroductional.setVisibility(View.VISIBLE);
            mIntroductional.setText(getString(R.string.link_text));
            mLoginInstead.setVisibility(View.INVISIBLE);
            mNoAccount.setVisibility(View.GONE);
        } else {
            mIntroductional.setVisibility(isIntroductional ? View.VISIBLE : View.GONE);
        }
        mLogInLayout.setVisibility(isLogin ? View.VISIBLE : View.GONE);
        mSignUpLayout.setVisibility(isLogin ? View.GONE : View.VISIBLE);
        mNoAccount.setVisibility(isLogin ? View.GONE : View.VISIBLE);
        mSignUpEmailField.addTextChangedListener(this);
        mSignUpPasswordField.addTextChangedListener(this);
        mLoginEmailField.addTextChangedListener(this);
        mLoginPasswordField.addTextChangedListener(this);
        mAgreement.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                String email = String.valueOf(mSignUpEmailField.getText());
                String password = String.valueOf(mSignUpPasswordField.getText());
                if (!email.equals("") && !password.equals("")  && password.length() > 7 && b) {
                    mSignUpButton.setEnabled(true);
                } else {
                    mSignUpButton.setEnabled(false);
                }
            }
        });
    }

    @OnClick(R.id.terms_privacy)
    void onTermsClicked() {
        goToFragment(new LegalPrivacyFragment(), true);
    }

    @OnClick({R.id.login, R.id.sign_up})
    void onSwitchViewClicked(View view) {
        if (view.getId() == R.id.login) {
            if (!isIntroductional && !isLinkAccount) {
                mTitle.setText(getString(R.string.login));
            }
            isLogin = true;
            mLogInLayout.setVisibility(View.VISIBLE);
            mSignUpLayout.setVisibility(View.GONE);
            mNoAccount.setVisibility(View.GONE);
        } else {
            if (!isIntroductional && !isLinkAccount) {
                mTitle.setText(getString(R.string.sign_up));
            }
            isLogin = false;
            mLogInLayout.setVisibility(View.GONE);
            mSignUpLayout.setVisibility(View.VISIBLE);
            mNoAccount.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.login_google)
    void loginGoogle() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, isLinkAccount ? RC_LINK : RC_SIGN_IN);
    }

    @OnClick(R.id.btn_log_in)
    void onLoginClicked() {
        hideSoftKeyboard();
        String email = String.valueOf(mLoginEmailField.getText());
        String password = String.valueOf(mLoginPasswordField.getText());
        boolean emailValid = android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        if (!emailValid) {
            mLoginEmailLayout.setError(getString(R.string.invalid_email));
            return;
        }
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(Objects.requireNonNull(getActivity()), task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInWithEmail:success");
                        FirebaseUser user = mAuth.getCurrentUser();
                        PreferenceUtils.getInstance().setFirstRun(false);
                        PreferenceUtils.getInstance().setHasSignIn(true);
                        goToFragment(new OverviewFragment(), false);
                        ((MainActivity) getActivity()).adVisibility(true);
                        //updateUI(user);
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "signInWithEmail:failure", task.getException());
                        Toast.makeText(getActivity(), "Incorrect email or password.", Toast.LENGTH_SHORT).show();
                        mLoginEmailLayout.setError(" ");
                        mLoginPasswordLayout.setError(" ");
                        //updateUI(null);
                    }
                });
    }

    @OnClick(R.id.btn_sign_up)
    void onSignUpClicked() {
        hideSoftKeyboard();
        String email = String.valueOf(mSignUpEmailField.getText());
        String password = String.valueOf(mSignUpPasswordField.getText());
        boolean emailValid = android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        if (!emailValid) {
            mSignUpEmailLayout.setError("Email address is not valid.");
            return;
        }
        if (isLinkAccount) {
            // Create EmailAuthCredential with email and password
            AuthCredential credential = EmailAuthProvider.getCredential(email, password);

            if (mAuth.getCurrentUser() != null) {
                mAuth.getCurrentUser().linkWithCredential(credential)
                        .addOnCompleteListener(Objects.requireNonNull(getActivity()), task -> {
                            if (task.isSuccessful()) {
                                Log.d(TAG, "linkWithCredential:success");
                                onBackClicked();
                            } else {
                                Log.w(TAG, "onlinkWithCredential:failure", task.getException());
                                Crashlytics.logException(task.getException());
                                Toast.makeText(getActivity(), "Authentication failed.",
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
            } else {
                Crashlytics.log(Log.ERROR, TAG, "mAuth.getCurrentUser is null");
                Toast.makeText(getActivity(), "Authentication failed. Please try again later.",
                        Toast.LENGTH_SHORT).show();
            }
        } else {
            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(Objects.requireNonNull(getActivity()), task -> {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            PreferenceUtils.getInstance().setFirstRun(false);
                            PreferenceUtils.getInstance().setHasSignIn(true);
                            goToFragment(new OverviewFragment(), false);
                            ((MainActivity) getActivity()).adVisibility(true);
                            //updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Crashlytics.logException(task.getException());
                            //Toast.makeText(getActivity(), "Authentication failed.", Toast.LENGTH_SHORT).show();
                            mSignUpEmailLayout.setError(task.getException().getLocalizedMessage());

                            //updateUI(null);
                        }
                    });
        }
    }

    @OnClick(R.id.no_account)
    void loginAnonymously() {
        mAuth.signInAnonymously()
                .addOnCompleteListener(Objects.requireNonNull(getActivity()), task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInAnonymously:success");
                        PreferenceUtils.getInstance().setAnonymous(true);
                        PreferenceUtils.getInstance().setFirstRun(false);
                        PreferenceUtils.getInstance().setHasSignIn(true);
                        goToFragment(new OverviewFragment(), false);
                        ((MainActivity) getActivity()).adVisibility(true);
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "signInAnonymously:failure", task.getException());
                        Crashlytics.logException(task.getException());
                        Toast.makeText(getActivity(), "Authentication failed.",
                                Toast.LENGTH_SHORT).show();
                    }
                });

    }

    @OnClick(R.id.forgot_password)
    void forgotPassword() {
        StandardDialog mDialog = StandardDialog.getInstance(getString(R.string.forgot_password_title),
                getString(R.string.forgot_password_text), getString(R.string.email_me));
        mDialog.setDialogClickListener(new StandardDialog.OnDialogClickListener() {
            @Override
            public void onCancelClick(StandardDialog dialog) {
                dialog.dismiss();
            }

            @Override
            public void onSubmitClick(StandardDialog dialog, String email) {
                mAuth.sendPasswordResetEmail(email)
                        .addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                Log.d(TAG, "Email sent.");
                                Toast.makeText(getActivity(), getString(R.string.email_sent),
                                        Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }
                        });
            }
        });
        mDialog.setCancelable(false);
        assert getFragmentManager() != null;
        mDialog.show(getFragmentManager(), null);
    }

    @OnClick(R.id.btn_back)
    void onBackClicked() {
        if (getActivity() != null) {
            getActivity().onBackPressed();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Crashlytics.logException(e);
                Log.w(TAG, "Google sign in failed", e);
                Toast.makeText(getActivity(), "Google sign in failed | " + e.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == RC_LINK) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(getActivity(), task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInWithCredential:success");
                        FirebaseUser user = mAuth.getCurrentUser();
                        PreferenceUtils.getInstance().setFirstRun(false);
                        PreferenceUtils.getInstance().setHasSignIn(true);
                        goToFragment(new OverviewFragment(), false);
                        ((MainActivity) getActivity()).adVisibility(true);
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "signInWithCredential:failure", task.getException());
                        Crashlytics.logException(task.getException());
                    }
                });
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
            if (mAuth.getCurrentUser() != null) {
                mAuth.getCurrentUser().linkWithCredential(credential)
                        .addOnCompleteListener(Objects.requireNonNull(getActivity()), task1 -> {
                            if (task1.isSuccessful()) {
                                Log.d(TAG, "linkWithCredential:success");
                                onBackClicked();
                            } else {
                                Log.w(TAG, "onlinkWithCredential:failure", task1.getException());
                                Crashlytics.logException(task1.getException());
                                Toast.makeText(getActivity(), "Authentication failed.",
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
            } else {
                Crashlytics.log(Log.ERROR, TAG, "mAuth.getCurrentUser is null");
                Toast.makeText(getActivity(), "Authentication failed. Please try again later.",
                        Toast.LENGTH_SHORT).show();
            }
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Crashlytics.logException(e);
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (isLogin) {
            mLoginEmailLayout.setError(null);
            mLoginPasswordLayout.setError(null);
            String email = String.valueOf(mLoginEmailField.getText());
            String password = String.valueOf(mLoginPasswordField.getText());
            if (!email.equals("") && !password.equals("") && password.length() > 7) {
                mLoginButton.setEnabled(true);
            } else {
                mLoginButton.setEnabled(false);
            }
        } else {
            mSignUpEmailLayout.setError(null);
            String email = String.valueOf(mSignUpEmailField.getText());
            String password = String.valueOf(mSignUpPasswordField.getText());
            boolean agreement = mAgreement.isChecked();
            if (!email.equals("") && !password.equals("")  && password.length() > 7 && agreement) {
                mSignUpButton.setEnabled(true);
            } else {
                mSignUpButton.setEnabled(false);
            }
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
