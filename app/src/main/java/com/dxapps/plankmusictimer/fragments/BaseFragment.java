package com.dxapps.plankmusictimer.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.dxapps.plankmusictimer.activities.MainActivity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public abstract class BaseFragment extends Fragment {
    public final static String TAG = BaseFragment.class.getName();

    public final static SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    /**
     * Go to a {@link Fragment}. Using {@link MainActivity#goToFragment(Fragment, boolean)}.
     *
     * @param fragment       Fragment to go to
     * @param addToBackStack Add to backstack
     */
    protected void goToFragment(Fragment fragment, boolean addToBackStack) {
        Activity activity = getActivity();
        if (activity != null) {
            if (activity instanceof MainActivity) {
                ((MainActivity) activity).goToFragment(fragment, addToBackStack);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    /**
     * Hides the soft keyboard
     */
    public void hideSoftKeyboard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        //getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public String msToString(long milliseconds) {
        SimpleDateFormat formatter = new SimpleDateFormat("mm:ss", Locale.getDefault());
        return formatter.format(new Date(milliseconds));
    }

}
