package com.dxapps.plankmusictimer.fragments;

import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.dxapps.plankmusictimer.R;
import com.dxapps.plankmusictimer.activities.MainActivity;
import com.dxapps.plankmusictimer.constants.BundleKeys;
import com.dxapps.plankmusictimer.layouts.StandardDialog;
import com.dxapps.plankmusictimer.models.Exercise;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerCallback;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.ui.views.YouTubePlayerSeekBar;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditExerciseFragment extends BaseFragment implements YouTubePlayerCallback, AppBarLayout.OnOffsetChangedListener {

    private final static String TAG = EditExerciseFragment.class.getName();

    @BindView(R.id.youtube_player_view) YouTubePlayerView mPlayerView;
    @BindView(R.id.youtube_player_seekbar) YouTubePlayerSeekBar mSeekBar;
    @BindView(R.id.main_collapsing) CollapsingToolbarLayout mToolbar;
    @BindView(R.id.main_appbar) AppBarLayout mAppBarLayout;
    @BindView(R.id.txt_title) TextView mTitle;
    @BindView(R.id.selected_time_text) TextView mSelectedTime;
    @BindView(R.id.confirm_button) Button mConfirmButton;
    @BindView(R.id.edit_duration) LinearLayout mEditDurationButton;
    @BindView(R.id.input_name) TextInputEditText mNameField;
    @BindView(R.id.input_url) TextInputEditText mUrlField;
    @BindView(R.id.start_text) TextView mStartTime;
    @BindView(R.id.timer) TextView mTimerText;
    @BindView(R.id.edit_start_time) LinearLayout mEditStartPositionButton;

    private YouTubePlayer mPlayer;
    private FirebaseFirestore mFirebaseDatabase;
    private Exercise mExerciseItem;
    private String mVideoId = "dQw4w9WgXcQ";
    private long mTimerAmount = 0;
    private long mStartPosition = 0;
    private long mTempStartPosition = 0;
    private boolean isShown = false;
    private boolean discardChanges = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static EditExerciseFragment newInstance(Exercise item) {
        EditExerciseFragment fragment = new EditExerciseFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(BundleKeys.ITEM, item);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ViewGroup view = (ViewGroup) inflater.inflate(R.layout.fragment_edit_exercise, container, false);
        ButterKnife.bind(this, view);
        mFirebaseDatabase = FirebaseFirestore.getInstance();
        mAppBarLayout.setExpanded(false, false); //Default
        mAppBarLayout.addOnOffsetChangedListener(this);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            mExerciseItem = (Exercise) bundle.getSerializable(BundleKeys.ITEM);
            mVideoId = mExerciseItem.getVideoId();
            mStartPosition = mExerciseItem.getStartTime();
            mTempStartPosition = mExerciseItem.getStartTime();
            mTimerAmount = mExerciseItem.getDuration();
        }

        mNameField.setText(mExerciseItem.getName());
        mUrlField.setText(getString(R.string.youtube_base_url, mExerciseItem.getVideoId()));
        mStartTime.setText(msToString(mExerciseItem.getStartTime()));
        mTimerText.setText(msToString(mExerciseItem.getDuration()));

        mPlayerView.getYouTubePlayerWhenReady(this);

        return view;
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        isShown = verticalOffset >= 0;
        if (!isShown) {
            if (mPlayer != null) {
                mPlayer.pause();
            }
        }
    }

    @OnClick(R.id.btn_save)
    void save(View view) {
        mVideoId = ((MainActivity) getActivity()).extractYTId(mUrlField.getText().toString());
        if (!mTimerText.getText().toString().equals("00:00") && mVideoId != null) {
            Map<String, Object> user = new HashMap<>();
            if (!mNameField.getText().toString().equals("")) {
                user.put("name", mNameField.getText().toString());
            } else {
                user.put("name", getString(R.string.default_title));
            }
            user.put("timeramount", mTimerAmount);
            user.put("videoid", mVideoId);
            user.put("starttime", mStartPosition);

            // Add a new document with a generated ID
            mFirebaseDatabase.collection("users").document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                    .collection("timers").document(mExerciseItem.getId())
                    .update(user)
                    .addOnSuccessListener(documentReference -> {
                        Log.d(TAG, "DocumentSnapshot added with ID: ");
                        Log.d("id", mVideoId);
                        Toast.makeText(view.getContext(), R.string.saved, Toast.LENGTH_SHORT).show();
                        backButton();
                    })
                    .addOnFailureListener(e -> Log.w(TAG, "Error adding document", e));
        } else {
            Toast.makeText(view.getContext(), R.string.warning_empty, Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.btn_delete)
    void delete() {
        StandardDialog mDialog = StandardDialog.getInstance(getString(R.string.delete_exercise),
                getString(R.string.delete_exercise_dialog), getString(R.string.delete));
        mDialog.setDialogClickListener(new StandardDialog.OnDialogClickListener() {
            @Override
            public void onCancelClick(StandardDialog dialog) {
                dialog.dismiss();
            }

            @Override
            public void onSubmitClick(StandardDialog dialog, String email) {
                dialog.dismiss();
                mFirebaseDatabase.collection("users").document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                        .collection("timers").document(mExerciseItem.getId())
                        .delete()
                        .addOnSuccessListener(documentReference -> {
                            Log.d(TAG, "DocumentSnapshot added with ID: ");
                            Log.d("id", mVideoId);
                            Toast.makeText(getContext(), R.string.deleted, Toast.LENGTH_SHORT).show();
                            backButton();
                        })
                        .addOnFailureListener(e -> Log.w(TAG, "Error adding document", e));
            }
        });
        mDialog.setCancelable(false);
        mDialog.show(getFragmentManager(), null);
    }

    @Override
    public void onStop() {
        super.onStop();
        mPlayerView.release();
    }

    @OnClick(R.id.btn_back)
    void backButton() {
        if (hasChanges()) {
            warningDialog();
        } else {
            if (getActivity() != null) {
                getActivity().onBackPressed();
            }
        }
    }

    @OnClick(R.id.edit_start_time)
    void onChangeStartTimeClicked() {
        mAppBarLayout.setExpanded(!isShown);
    }

    @OnClick(R.id.confirm_button)
    void onSubmitClick() {
        mStartPosition = mTempStartPosition;
        mStartTime.setText(msToString(mStartPosition));
        mAppBarLayout.setExpanded(false);
    }

    @OnClick(R.id.edit_duration)
    void showDialog() {
        View dialogView = getLayoutInflater().inflate(R.layout.timer_picker_dialog, null);
        NumberPicker.Formatter formatter = i -> String.format("%02d", i);
        final NumberPicker minutePicker = dialogView.findViewById(R.id.dialog_minute_picker);
        minutePicker.setMaxValue(30);
        minutePicker.setMinValue(0);
        minutePicker.setWrapSelectorWheel(false);
        minutePicker.setFormatter(formatter);

        NumberPicker.Formatter nFormatter = value -> {
            int temp = value * 5;
            return String.format("%02d", temp);
        };

        final NumberPicker secondPicker = dialogView.findViewById(R.id.dialog_second_picker);
        secondPicker.setMaxValue(11);
        secondPicker.setMinValue(0);
        secondPicker.setFormatter(nFormatter);
        secondPicker.setWrapSelectorWheel(false);

        new AlertDialog.Builder(getActivity())
                .setTitle(R.string.edit_duration)
                .setView(dialogView)
                .setPositiveButton(R.string.done, (dialogInterface, i) -> {
                    mTimerAmount = minutePicker.getValue() * 60000 + secondPicker.getValue() * 5000;
                    mTimerText.setText(msToString(mTimerAmount));
                })
                .setNegativeButton(R.string.cancel, (dialogInterface, i) -> {
                })
                .show();
    }

    @Override
    public void onYouTubePlayer(YouTubePlayer youTubePlayer) {
        float seconds = mStartPosition / 1000f;
        mPlayer = youTubePlayer;
        mPlayer.cueVideo(mVideoId, seconds);
        mSeekBar.setYoutubePlayerSeekBarListener(time -> {
            mPlayer.seekTo(time);
            Log.d(TAG, "On seek:" + time);
            mTempStartPosition = (long) time * 1000;
            mSelectedTime.setText(msToString(mTempStartPosition));
        });
        mPlayer.addListener(mSeekBar);
        mPlayer.addListener(new AbstractYouTubePlayerListener() {
            @Override
            public void onCurrentSecond(YouTubePlayer youTubePlayer, float second) {
                super.onCurrentSecond(youTubePlayer, second);
                Log.d(TAG, "On run:" + second);
                mTempStartPosition = (long) second * 1000;
                mSelectedTime.setText(msToString(mTempStartPosition));
            }
        });
    }

    public boolean hasChanges() {
        if (!discardChanges) {
            return !mNameField.getText().toString().equals(mExerciseItem.getName()) || mTempStartPosition != mStartPosition;
        } else {
            return false;
        }
    }

    public void warningDialog() {
        StandardDialog mDialog = StandardDialog.getInstance(getString(R.string.dismiss_exercise),
                getString(R.string.dismiss_exercise_dialog), getString(R.string.yes));
        mDialog.setDialogClickListener(new StandardDialog.OnDialogClickListener() {
            @Override
            public void onCancelClick(StandardDialog dialog) {
                dialog.dismiss();
            }

            @Override
            public void onSubmitClick(StandardDialog dialog, String email) {
                dialog.dismiss();
                mPlayerView.release();
                discardChanges = true;
                if (getActivity() != null) {
                    getActivity().onBackPressed();
                }
            }
        });
        mDialog.setCancelable(false);
        mDialog.show(getFragmentManager(), null);
    }
}
