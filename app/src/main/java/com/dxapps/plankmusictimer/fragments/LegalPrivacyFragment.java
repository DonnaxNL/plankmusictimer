package com.dxapps.plankmusictimer.fragments;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.dxapps.plankmusictimer.BuildConfig;
import com.dxapps.plankmusictimer.R;
import com.dxapps.plankmusictimer.activities.MainActivity;
import com.dxapps.plankmusictimer.layouts.StandardDialog;
import com.dxapps.plankmusictimer.utils.PreferenceUtils;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LegalPrivacyFragment extends BaseFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ViewGroup view = (ViewGroup) inflater.inflate(R.layout.fragment_legal_privacy, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @OnClick({R.id.btn_terms, R.id.btn_privacy, R.id.btn_license, R.id.btn_back})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_terms:
                goToFragment(WebFragment.newInstance(getString(R.string.terms_service), getString(R.string.terms_url)), true);
                break;
            case R.id.btn_privacy:
                goToFragment(WebFragment.newInstance(getString(R.string.privacy_policy), getString(R.string.privacy_url)), true);
                break;
            case R.id.btn_license:
                goToFragment(WebFragment.newInstance(getString(R.string.open_source), getString(R.string.open_source_url)), true);
                break;
            case R.id.btn_back:
                if (getActivity() != null) {
                    getActivity().onBackPressed();
                }
                break;
            default:
                break;
        }
    }
}
