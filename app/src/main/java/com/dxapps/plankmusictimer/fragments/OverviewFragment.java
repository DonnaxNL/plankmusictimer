package com.dxapps.plankmusictimer.fragments;

import android.app.AlertDialog;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dxapps.plankmusictimer.R;
import com.dxapps.plankmusictimer.activities.MainActivity;
import com.dxapps.plankmusictimer.adapters.ExerciseAdapter;
import com.dxapps.plankmusictimer.constants.BundleKeys;

import com.dxapps.plankmusictimer.constants.SortMethod;
import com.dxapps.plankmusictimer.layouts.StartTimeDialog;
import com.dxapps.plankmusictimer.models.Exercise;
import com.dxapps.plankmusictimer.utils.PreferenceUtils;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OverviewFragment extends BaseFragment implements AppBarLayout.OnOffsetChangedListener, StartTimeDialog.OnStartTimeDialogClickListener, ExerciseAdapter.ExerciseClickListener {

    private final static String TAG = OverviewFragment.class.getName();

    @BindView(R.id.coordinatorLayout) CoordinatorLayout coordinatorLayout;
    @BindView(R.id.main_collapsing) CollapsingToolbarLayout mToolbar;
    @BindView(R.id.main_appbar) AppBarLayout mAppBarLayout;
    @BindView(R.id.loading_bar) ProgressBar mLoading;
    @BindView(R.id.empty_list) TextView mEmptyList;
    @BindView(R.id.exercise_list) RecyclerView mExerciseList;
    @BindView(R.id.btn_new) ImageButton mAddTimer;
    @BindView(R.id.start_text) TextView mStartTime;
    @BindView(R.id.timer) TextView mTimerText;
    @BindView(R.id.btn_save) LinearLayout mSaveButton;
    @BindView(R.id.play) FloatingActionButton mPlayButton;
    @BindView(R.id.edit_duration) LinearLayout mEditDurationButton;
    @BindView(R.id.input_name) TextInputEditText mNameField;
    @BindView(R.id.input_url) TextInputEditText mUrlField;
    @BindView(R.id.edit_start_time) LinearLayout mEditStartPositionButton;

    private boolean isShown = false;
    private ExerciseAdapter mAdapter;
    private long mTimerAmount = 120000;
    private long mStartPosition = 0;
    private String mVideoId;
    private FirebaseFirestore mFirebaseDatabase;
    private List<Exercise> mItemList = new ArrayList<>();

    public static OverviewFragment newInstance(String vId) {
        OverviewFragment fragment = new OverviewFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(BundleKeys.SHARED_VIDEO_ID, vId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ViewGroup view = (ViewGroup) inflater.inflate(R.layout.fragment_overview, container, false);
        ButterKnife.bind(this, view);
        mFirebaseDatabase = FirebaseFirestore.getInstance();

        mAppBarLayout.setExpanded(false, false); //Default
        mAppBarLayout.addOnOffsetChangedListener(this);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey(BundleKeys.SHARED_VIDEO_ID)) {
                mAppBarLayout.setExpanded(true);
                mUrlField.setText((String) bundle.getSerializable(BundleKeys.SHARED_VIDEO_ID));
                hideSoftKeyboard();
            }
        }

        mAdapter = new ExerciseAdapter(getContext(), mItemList);
        mAdapter.setListener(this);
        mAdapter.notifyDataSetChanged();
        mExerciseList.setAdapter(mAdapter);

        initExerciseList();
        ((MainActivity) getActivity()).adVisibility(true);
        hideSoftKeyboard();
        return view;
    }

    private void initExerciseList() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        mExerciseList.setLayoutManager(layoutManager);

        mAdapter = new ExerciseAdapter(getContext(), mItemList);
        mAdapter.setListener(this);
        mExerciseList.setAdapter(mAdapter);
        getData();
    }

    private void getData() {
        Query.Direction direction = Query.Direction.DESCENDING;
        if (PreferenceUtils.getInstance().getSortDirection().equals(SortMethod.SORT_DIRECTION_ASC)) {
            direction = Query.Direction.ASCENDING;
        }
        mFirebaseDatabase.collection("users")
                .document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .collection("timers")
                .orderBy(PreferenceUtils.getInstance().getSortBy(), direction)
                .addSnapshotListener((queryDocumentSnapshots, e) -> {
                    if (e != null) {
                        Log.w(TAG, "Listen failed.", e);
                        return;
                    }
                    if (queryDocumentSnapshots.getDocuments().size() > 0) {
                        mItemList.clear();
                        for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                            if (document.exists()){
                                mItemList.add(new Exercise(document.getId(),
                                        (String) document.get("name"),
                                        (String) document.get("videoid"),
                                        (long) document.get("starttime"),
                                        (long) document.get("timeramount")));
                                mAdapter.notifyDataSetChanged();
                            }
                        }
                        mExerciseList.setVisibility(View.VISIBLE);
                        mLoading.setVisibility(View.GONE);
                        Log.d("YourTag", "list: " + mItemList);
                    } else {
                        mLoading.setVisibility(View.INVISIBLE);
                        mEmptyList.setVisibility(View.VISIBLE);
                        mAppBarLayout.setExpanded(true);
                    }
                });
    }

    private void resetFields() {
        mNameField.setText("");
        mUrlField.setText("");
        mStartPosition = 0;
        mTimerAmount = 0;
    }

    @OnClick(R.id.btn_new)
    void onAddTimerClicked() {
        Drawable drawable = mAddTimer.getDrawable();
        //drawable
        if (drawable instanceof Animatable) {
            ((Animatable) drawable).start();
        }
        mAppBarLayout.setExpanded(!isShown);
        Log.d(TAG, "Before: " + String.valueOf(isShown));
    }

    @OnClick(R.id.edit_duration)
    void showDialog() {
        View dialogView = getLayoutInflater().inflate(R.layout.timer_picker_dialog, null);
        NumberPicker.Formatter formatter = i -> String.format("%02d", i);
        final NumberPicker minutePicker = dialogView.findViewById(R.id.dialog_minute_picker);
        minutePicker.setMaxValue(30);
        minutePicker.setMinValue(0);
        minutePicker.setWrapSelectorWheel(false);
        minutePicker.setFormatter(formatter);

        NumberPicker.Formatter nFormatter = value -> {
            int temp = value * 5;
            return String.format("%02d", temp);
        };

        final NumberPicker secondPicker = dialogView.findViewById(R.id.dialog_second_picker);
        secondPicker.setMaxValue(11);
        secondPicker.setMinValue(0);
        secondPicker.setFormatter(nFormatter);
        secondPicker.setWrapSelectorWheel(false);

        new AlertDialog.Builder(getActivity())
                .setTitle(R.string.edit_duration)
                .setView(dialogView)
                .setPositiveButton(R.string.done, (dialogInterface, i) -> {
                    mTimerAmount = minutePicker.getValue() * 60000 + secondPicker.getValue() * 5000;
                    mTimerText.setText(msToString(mTimerAmount));
                })
                .setNegativeButton(R.string.cancel, (dialogInterface, i) -> {
                })
                .show();
    }

    @OnClick({R.id.btn_save, R.id.play})
    void save(View view) {
        mVideoId = ((MainActivity) getActivity()).extractYTId(mUrlField.getText().toString());
        if (!mTimerText.getText().toString().equals("00:00") && mVideoId != null) {
            Map<String, Object> user = new HashMap<>();
            if (!mNameField.getText().toString().equals("")) {
                user.put("name", mNameField.getText().toString());
            } else {
                user.put("name", getString(R.string.default_title));
            }
            user.put("timeramount", mTimerAmount);
            user.put("videoid", mVideoId);
            user.put("starttime", mStartPosition);
            user.put("created", FieldValue.serverTimestamp());
            user.put("last_used", FieldValue.serverTimestamp());

            // Add a new document with a generated ID
            mFirebaseDatabase.collection("users").document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                    .collection("timers").document()
                    .set(user)
                    .addOnSuccessListener(documentReference -> {
                        Log.d(TAG, "DocumentSnapshot added with ID: ");
                        Log.d("id", mVideoId);
                        if (view.getId() == R.id.btn_save) {
                            getData();
                            resetFields();
                            mAppBarLayout.setExpanded(false);
                        } else {
                            ((MainActivity) getActivity()).adVisibility(false);
                            goToFragment(TimerFragment.newInstance(mVideoId, mTimerAmount, mStartPosition), true);
                        }
                    })
                    .addOnFailureListener(e -> Log.w(TAG, "Error adding document", e));
        } else {
            Toast.makeText(getContext(), R.string.warning_empty, Toast.LENGTH_SHORT).show();
            //Snackbar.make(coordinatorLayout, R.string.warning_empty, Snackbar.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.edit_start_time)
    void editStartPosition() {
        String vId = ((MainActivity) getActivity()).extractYTId(mUrlField.getText().toString());
        if (vId != null) {
            StartTimeDialog mStartTimeDialog = StartTimeDialog.getInstance(vId);
            mStartTimeDialog.setStartTimeDialogClickListener(this);
            mStartTimeDialog.setCancelable(false);
            mStartTimeDialog.show(getFragmentManager(), null);
        } else {
            Toast.makeText(getContext(), R.string.warning_empty, Toast.LENGTH_SHORT).show();
            //Snackbar.make(coordinatorLayout, R.string.warning_empty, Snackbar.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.btn_settings)
    void settings() {
        ((MainActivity) getActivity()).adVisibility(false);
        goToFragment(new SettingsFragment(), true);
    }

    @Override
    public void onCancelClick(StartTimeDialog dialog) {
        dialog.dismiss();
    }

    @Override
    public void onSubmitClick(StartTimeDialog dialog, int startPosition) {
        mStartPosition = startPosition;
        mStartTime.setText(msToString(mStartPosition));
        dialog.dismiss();
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int offset) {
        isShown = offset >= 0;
        if (!isShown) {
            mAddTimer.setImageResource(R.drawable.add_close);
            mEmptyList.setText(getString(R.string.empty_list_closed));
        } else {
            mAddTimer.setImageResource(R.drawable.close_add);
            mEmptyList.setText(getString(R.string.empty_list));
        }
    }

    @Override
    public void onEditExerciseItemClick(View itemView, int position, Exercise selectedItem) {
        goToFragment(EditExerciseFragment.newInstance(selectedItem), true);
    }

    @Override
    public void onExerciseItemClick(View itemView, int position, Exercise selectedItem) {
        Map<String, Object> user = new HashMap<>();
        user.put("last_used", FieldValue.serverTimestamp());

        mFirebaseDatabase.collection("users")
                .document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .collection("timers").document(selectedItem.getId())
                .update(user)
                .addOnSuccessListener(documentReference -> {
                    ((MainActivity) getActivity()).adVisibility(false);
                    goToFragment(TimerFragment.newInstance(
                            selectedItem.getVideoId(),
                            selectedItem.getDuration(),
                            selectedItem.getStartTime()), true);
                })
                .addOnFailureListener(e -> Log.w(TAG, "Error updating document", e));
    }
}
