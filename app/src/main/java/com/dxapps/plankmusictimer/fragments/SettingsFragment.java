package com.dxapps.plankmusictimer.fragments;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dxapps.plankmusictimer.R;
import com.dxapps.plankmusictimer.BuildConfig;
import com.dxapps.plankmusictimer.activities.MainActivity;
import com.dxapps.plankmusictimer.constants.SortMethod;
import com.dxapps.plankmusictimer.layouts.StandardDialog;
import com.dxapps.plankmusictimer.utils.PreferenceUtils;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;

public class SettingsFragment extends BaseFragment{

    @BindView(R.id.sort) Spinner mSortSpinner;
    @BindView(R.id.btn_link_account) LinearLayout mBtnLinkAccount;
    @BindView(R.id.countdown_duration) TextView mCountdownDuration;
    @BindView(R.id.version_number) TextView mVersionNumber;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ViewGroup view = (ViewGroup) inflater.inflate(R.layout.fragment_settings, container, false);
        ButterKnife.bind(this, view);

        if (!PreferenceUtils.getInstance().isAnonymous()) {
            mBtnLinkAccount.setVisibility(View.GONE);
        }
        mCountdownDuration.setText(getString(R.string.countdown_seconds, (PreferenceUtils.getInstance().getCountdownDuration()/1000)));
        mVersionNumber.setText(BuildConfig.VERSION_NAME);

        // Spinner Drop down elements
        List<String> sortBy = new ArrayList<>();
        sortBy.add(getString(R.string.sort_list_last_used));
        sortBy.add(getString(R.string.sort_list_newest));
        sortBy.add(getString(R.string.sort_list_oldest));
        sortBy.add(getString(R.string.sort_list_name_az));
        sortBy.add(getString(R.string.sort_list_name_za));

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, sortBy);

        // Drop down layout style - list view with radio button
        //dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        mSortSpinner.setAdapter(dataAdapter);
        mSortSpinner.setSelection(PreferenceUtils.getInstance().getSortId());

        return view;
    }

    @OnItemSelected(R.id.sort)
    void onSortSelect(Spinner spinner, int position) {
        PreferenceUtils.getInstance().setSortId(position);
        switch (position) {
            case 0:
                PreferenceUtils.getInstance().setSortBy(SortMethod.SORT_LAST_USED);
                PreferenceUtils.getInstance().setSortDirection(SortMethod.SORT_DIRECTION_DESC);
                break;
            case 1:
                PreferenceUtils.getInstance().setSortBy(SortMethod.SORT_CREATED);
                PreferenceUtils.getInstance().setSortDirection(SortMethod.SORT_DIRECTION_DESC);
                break;
            case 2:
                PreferenceUtils.getInstance().setSortBy(SortMethod.SORT_CREATED);
                PreferenceUtils.getInstance().setSortDirection(SortMethod.SORT_DIRECTION_ASC);
                break;
            case 3:
                PreferenceUtils.getInstance().setSortBy(SortMethod.SORT_NAME);
                PreferenceUtils.getInstance().setSortDirection(SortMethod.SORT_DIRECTION_ASC);
                break;
            case 4:
                PreferenceUtils.getInstance().setSortBy(SortMethod.SORT_NAME);
                PreferenceUtils.getInstance().setSortDirection(SortMethod.SORT_DIRECTION_DESC);
                break;
            default:
                break;
        }
    }

    @OnClick({R.id.btn_countdown_duration, R.id.btn_sort_list, R.id.btn_link_account, R.id.btn_legal_privacy,
              R.id.btn_contact, R.id.btn_rate, R.id.btn_share, R.id.btn_logout, R.id.btn_back})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_countdown_duration:
                showDialog();
                break;
            case R.id.btn_sort_list:
                mSortSpinner.performClick();
                //Toast.makeText(getActivity(), "Coming soon", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_link_account:
                goToFragment(AccountFragment.newInstance(false, false, true), true);
                break;
            case R.id.btn_legal_privacy:
                goToFragment(new LegalPrivacyFragment(), true);
                break;
            case R.id.btn_contact:
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                String mailto = "mailto:dxappsmail@gmail.com" +
                        "&subject=" + Uri.encode("PlankMusicTimer - Feedback") +
                        "&body=" + Uri.encode("App version: " + BuildConfig.VERSION_NAME);
                intent.setData(Uri.parse(mailto));
                startActivity(Intent.createChooser(intent, getString(R.string.send_email)));
                break;
            case R.id.btn_rate:
                Uri uri = Uri.parse("market://details?id=" + getContext().getPackageName());
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                // To count with Play market backstack, After pressing back button,
                // to taken back to our application, we need to add following flags to intent.
                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                        Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                try {
                    startActivity(goToMarket);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=" + getContext().getPackageName())));
                }
                break;
            case R.id.btn_share:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        "Hey check out this app at: https://play.google.com/store/apps/details?id=" + getContext().getPackageName());
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                break;
            case R.id.btn_logout:
                signOff();
                break;
            case R.id.btn_back:
                if (getActivity() != null) {
                    getActivity().onBackPressed();
                }
                break;
            default:
                break;
        }
    }

    private void showDialog() {
        View dialogView = getLayoutInflater().inflate(R.layout.countdown_picker_dialog, null);
        NumberPicker.Formatter formatter = i -> String.format("%02d", i);
        final NumberPicker secondPicker = dialogView.findViewById(R.id.dialog_second_picker);
        secondPicker.setMaxValue(60);
        secondPicker.setMinValue(3);
        secondPicker.setWrapSelectorWheel(true);
        secondPicker.setFormatter(formatter);
        secondPicker.setValue(PreferenceUtils.getInstance().getCountdownDuration()/1000);

        new AlertDialog.Builder(getActivity())
                .setTitle(R.string.countdown_duration)
                .setView(dialogView)
                .setPositiveButton(R.string.done, (dialogInterface, i) -> {
                    PreferenceUtils.getInstance().setCountdownDuration(secondPicker.getValue() * 1000);
                    mCountdownDuration.setText(getString(R.string.countdown_seconds, secondPicker.getValue()));
                })
                .setNegativeButton(R.string.cancel, (dialogInterface, i) -> {
                })
                .show();
    }

    private void signOff() {
        if (getActivity() != null) {
            String text = getString(R.string.sign_off_dialog);
            if (PreferenceUtils.getInstance().isAnonymous()) {
                text = getString(R.string.sign_off_anon_dialog);
            }
            StandardDialog mDialog = StandardDialog.getInstance(getString(R.string.sign_off), text, "");
            mDialog.setDialogClickListener(new StandardDialog.OnDialogClickListener() {
                @Override
                public void onCancelClick(StandardDialog dialog) {
                    dialog.dismiss();
                    getActivity().finish();
                }

                @Override
                public void onSubmitClick(StandardDialog dialog, String email) {
                    FirebaseAuth.getInstance().signOut();
                    PreferenceUtils.getInstance().resetPreferences();
                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                }
            });
            mDialog.setCancelable(false);
            mDialog.show(getFragmentManager(), null);
        }
    }
}
