package com.dxapps.plankmusictimer.fragments;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.dxapps.plankmusictimer.R;
import com.dxapps.plankmusictimer.activities.MainActivity;
import com.dxapps.plankmusictimer.constants.BundleKeys;
import com.dxapps.plankmusictimer.models.Quote;
import com.dxapps.plankmusictimer.utils.PreferenceUtils;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.PlayerConstants;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerCallback;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TimerFragment extends BaseFragment implements YouTubePlayerCallback, AppBarLayout.OnOffsetChangedListener {

    private final static String TAG = TimerFragment.class.getName();

    @BindView(R.id.youtube_player_view) YouTubePlayerView mPlayerView;
    @BindView(R.id.main_collapsing) CollapsingToolbarLayout mToolbar;
    @BindView(R.id.main_appbar) AppBarLayout mAppBarLayout;
    @BindView(R.id.txt_title) TextView mTitle;
    @BindView(R.id.timer) TextView mTimerText;
    @BindView(R.id.btn_pause_start) Button mStartPauseButton;
    @BindView(R.id.countdown) TextView mCountdownText;
    @BindView(R.id.timer_bar) ProgressBar mTimerBar;
    @BindView(R.id.quote) TextView mQuote;
    //@BindView(R.id.adView) AdView mAdBar;

    private YouTubePlayer mPlayer;
    private CountDownTimer mCountdown;
    private CountDownTimer mTimer;
    private FirebaseFirestore mFirebaseDatabase;
    private String mVideoId = "0ZLjp5Rei4s";
    private long mTimerAmount = 0;
    private long mStartPosition = 0;
    private boolean mTimerStarted = false;
    private int scrollRange = -1;
    private boolean isShow;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static TimerFragment newInstance(String vId, long timerAmount, long startPosition) {
        TimerFragment fragment = new TimerFragment();
        Bundle bundle = new Bundle();
        bundle.putString(BundleKeys.VIDEO_ID, vId);
        bundle.putLong(BundleKeys.TIMER, timerAmount);
        bundle.putLong(BundleKeys.START_TIME, startPosition);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ViewGroup view = (ViewGroup) inflater.inflate(R.layout.fragment_timer, container, false);
        ButterKnife.bind(this, view);
        mFirebaseDatabase = FirebaseFirestore.getInstance();
        mAppBarLayout.addOnOffsetChangedListener(this);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            mVideoId = bundle.getString(BundleKeys.VIDEO_ID);
            mTimerAmount = bundle.getLong(BundleKeys.TIMER);
            mStartPosition = bundle.getLong(BundleKeys.START_TIME);
        }

        getQuote();
        mPlayerView.getYouTubePlayerWhenReady(this);
        setCountdown(PreferenceUtils.getInstance().getCountdownDuration());
        mCountdownText.setVisibility(View.VISIBLE);
        mTimerBar.setMax((int) mTimerAmount);
        mTimerBar.setProgress((int) mTimerAmount);

        return view;
    }

    private void getQuote() {
        List<Quote> quoteList = PreferenceUtils.getInstance().getQuotes();
        //Log.d(TAG, "list is: " + quoteList.size());
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        database.getReference("quotes").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                Long value = dataSnapshot.getValue(Long.class);
                Log.d(TAG, "Value is: " + value);
                if (quoteList == null || value == null || quoteList.size() != value) {
                    Log.d(TAG, "Get all quotes");
                    mFirebaseDatabase.
                            collection("quotes").get()
                            .addOnCompleteListener(task -> {
                                List<Quote> quoteList = new ArrayList<>();
                                int size;

                                if (task.isSuccessful()) {
                                    if (task.getResult() != null) {
                                        size = task.getResult().getDocuments().size();
                                        for (DocumentSnapshot document : task.getResult().getDocuments()) {
                                            Log.d(TAG, document.getId() + " => " + document.getData());
                                            quoteList.add(new Quote((String) document.get("quote"),
                                                    (String) document.get("author")));
                                        }
                                        PreferenceUtils.getInstance().setQuotes(quoteList);
                                        Random random = new Random();
                                        mQuote.setText(getResources().getString(R.string.quote, quoteList.get(random.nextInt(size)).getQuote()));
                                    }
                                } else {
                                    Log.d(TAG, "Error getting documents: ", task.getException());
                                }
                            });
                } else {
                    Log.d(TAG, "Quotes from pref");
                    int size = PreferenceUtils.getInstance().getQuotes().size();
                    List<Quote> quoteList = PreferenceUtils.getInstance().getQuotes();
                    Random random = new Random();
                    mQuote.setText(getResources().getString(R.string.quote, quoteList.get(random.nextInt(size)).getQuote()));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
    }

    private void setCountdown(int timerAmount) {
        mCountdown = new CountDownTimer(timerAmount, 1000) {

            public void onTick(long millisUntilFinished) {
                long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished));
                mCountdownText.setText(String.valueOf(seconds+1));
            }

            public void onFinish() {
                if (mPlayer != null) {
                    mPlayer.play();
                    if (getActivity() != null) {
                        ((MainActivity) getActivity()).adVisibility(true);
                    }
                    mCountdownText.setVisibility(View.GONE);
                }
            }
        };
        mCountdown.start();
    }

    private void setTimer(long timerAmount) {
        mStartPauseButton.setText(getResources().getString(R.string.pause));
        mTimer = new CountDownTimer(timerAmount, 1) {

            public void onTick(long millisUntilFinished) {
                if (getActivity() == null) {
                    mPlayerView.release();
                } else {
                    mTitle.setText(getResources().getString(R.string.time_remaining, msToString((int) millisUntilFinished)));
                    mTimerText.setText(msToString((int) millisUntilFinished));
                    mTimerBar.setProgress((int) millisUntilFinished);
                    mTimerAmount = (int) millisUntilFinished;
                    //Log.d(TAG, String.valueOf(mTimerAmount));
                }
            }

            public void onFinish() {
                mTitle.setText(getResources().getString(R.string.done));
                mTimerStarted = false;
                mPlayer.pause();
                new Handler().postDelayed(() -> onBackPressed(), 2000);
            }
        };

        mTimer.start();
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (scrollRange == -1) {
            scrollRange = appBarLayout.getTotalScrollRange();
        }
        int positive = scrollRange + verticalOffset;
        float position = verticalOffset *0.85f;

        Log.d("Scroll", scrollRange + " | " + verticalOffset + " | " + position);
        if (positive > 100) {
            mPlayerView.setTranslationX(position);
            mPlayerView.setScaleX((positive * 0.156f) / 100);
            mPlayerView.setScaleY((positive * 0.156f) / 100);
        }

        if (scrollRange + verticalOffset < 100) {
            mTitle.setVisibility(View.VISIBLE);
            mTimerText.setVisibility(View.GONE);
            isShow = true;
        } else if(isShow) {
            mTitle.setVisibility(View.GONE);
            mTimerText.setVisibility(View.VISIBLE);
            isShow = false;
        }
    }

    @OnClick({R.id.btn_pause_start, R.id.btn_stop})
    void onButtonPressed(View view) {
        switch(view.getId()) {
            case R.id.btn_pause_start:
                if (!mTimerStarted) {
                    mPlayer.play();
                    mStartPauseButton.setText(getResources().getString(R.string.pause));
                    mTimerStarted = true;
                } else {
                    mPlayer.pause();
                    mStartPauseButton.setText(getResources().getString(R.string.resume));
                    mTimer.cancel();
                    mTimerStarted = false;
                }
                break;
            case R.id.btn_stop:
                onBackPressed();
                break;
            default:
                break;
        }
    }

    private void onBackPressed() {
        if (mTimer != null) {
            mTimer.cancel();
        }
        //mNotificationManager.cancel(01);
        mPlayerView.release();
        if (getActivity() != null) {
            getActivity().onBackPressed();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mTimer != null) {
            mTimer.cancel();
        }
        mPlayerView.release();
    }

    @Override
    public void onYouTubePlayer(@NonNull YouTubePlayer youTubePlayer) {
        float seconds = mStartPosition / 1000f;
        mPlayer = youTubePlayer;
        mPlayer.cueVideo(mVideoId, seconds);
        mPlayer.addListener(new AbstractYouTubePlayerListener() {
            @Override
            public void onStateChange(@NonNull YouTubePlayer youTubePlayer, @NonNull PlayerConstants.PlayerState state) {
                super.onStateChange(youTubePlayer, state);
                if (state == PlayerConstants.PlayerState.PLAYING) {
                    setTimer(mTimerAmount);
                    mStartPauseButton.setText(getResources().getString(R.string.pause));
                    mTimerStarted = true;
                } else if (state == PlayerConstants.PlayerState.PAUSED ||
                           state == PlayerConstants.PlayerState.BUFFERING && mTimer != null) {
                    mStartPauseButton.setText(getResources().getString(R.string.resume));
                    mTimer.cancel();
                    mTimerStarted = false;
                }
            }
        });
    }
}
