package com.dxapps.plankmusictimer.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import com.dxapps.plankmusictimer.R;
import com.dxapps.plankmusictimer.constants.BundleKeys;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WebFragment extends BaseFragment {

    @BindView(R.id.txt_title) TextView mTitle;
    @BindView(R.id.web_view) WebView mWebView;

    private String mUrl;

    public static WebFragment newInstance(String title, String url) {
        WebFragment fragment = new WebFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(BundleKeys.WEB_TITLE, title);
        bundle.putSerializable(BundleKeys.WEB_URL, url);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ViewGroup view = (ViewGroup) inflater.inflate(R.layout.fragment_web, container, false);
        ButterKnife.bind(this, view);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey(BundleKeys.WEB_TITLE)) {
                mTitle.setText((String) bundle.getSerializable(BundleKeys.WEB_TITLE));
                mUrl = (String) bundle.getSerializable(BundleKeys.WEB_URL);
            }
        }

        if (mUrl != null) {
            mWebView.loadUrl(mUrl);
        }

        return view;
    }

    @OnClick(R.id.btn_back)
    void onBackClicked() {
        if (getActivity() != null) {
            getActivity().onBackPressed();
        }
    }
}
