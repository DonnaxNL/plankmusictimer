package com.dxapps.plankmusictimer.layouts;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.dxapps.plankmusictimer.R;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerCallback;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.ui.utils.TimeUtilities;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.ui.views.YouTubePlayerSeekBar;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StandardDialog extends DialogFragment implements View.OnClickListener{

    private static final String EXTRA_DIALOG_TITLE = "dialog_title";
    private static final String EXTRA_DIALOG_TEXT = "dialog_text";
    private static final String EXTRA_DIALOG_BUTTON = "dialog_button";
    private OnDialogClickListener mOnClickListener;

    @BindView(R.id.dialog_title) TextView mTitle;
    @BindView(R.id.dialog_text) TextView mText;
    @BindView(R.id.email_input) EditText mEmailInput;
    @BindView(R.id.divider) View mDivider;
    @BindView(R.id.cancel_button) Button mCancelButton;
    @BindView(R.id.submit_button) Button mSubmitButton;

    private String title;
    private String text;
    private String buttonText;

    public static StandardDialog getInstance(String title, String text, String buttonText) {
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_DIALOG_TITLE, title);
        bundle.putString(EXTRA_DIALOG_TEXT, text);
        bundle.putString(EXTRA_DIALOG_BUTTON, buttonText);

        StandardDialog fragment = new StandardDialog();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            title = bundle.getString(EXTRA_DIALOG_TITLE, "");
            text = bundle.getString(EXTRA_DIALOG_TEXT, "");
            buttonText = bundle.getString(EXTRA_DIALOG_BUTTON, "");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Objects.requireNonNull(getDialog().getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.error_message_background);
        View view = inflater.inflate(R.layout.standard_dialog, container, false);
        ButterKnife.bind(this, view);

        mTitle.setText(title);
        mText.setText(text);

        if (title.equals(getString(R.string.no_internet_title))) {
            mCancelButton.setText(buttonText);
            mDivider.setVisibility(View.GONE);
            mSubmitButton.setVisibility(View.GONE);
        } else if (title.equals(getString(R.string.forgot_password_title))) {
            mEmailInput.setVisibility(View.VISIBLE);
            mSubmitButton.setText(buttonText);
        } else if (!buttonText.equals("")) {
            mSubmitButton.setText(buttonText);
            if (buttonText.equals(getString(R.string.delete)) || buttonText.equals(getString(R.string.yes))) {
                mSubmitButton.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.red));
                mSubmitButton.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
            }
        }

        return view;
    }


    @Override
    public void show(FragmentManager manager, String tag) {
        FragmentTransaction ft = manager.beginTransaction();
        ft.add(this, tag);
        ft.commitAllowingStateLoss();
    }

    public void setDialogClickListener(OnDialogClickListener listener) {
        mOnClickListener = listener;
    }

    @OnClick({R.id.cancel_button, R.id.submit_button})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancel_button:
                if (mOnClickListener != null) {
                    mOnClickListener.onCancelClick(StandardDialog.this);
                }
                break;
            case R.id.submit_button:
                if (mOnClickListener != null) {
                    String email = String.valueOf(mEmailInput.getText());
                    if (mEmailInput.getVisibility() == View.VISIBLE && email.length() > 0) {
                        boolean emailValid = android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
                        if (emailValid) {
                            mOnClickListener.onSubmitClick(StandardDialog.this, email);
                        } else {
                            Toast.makeText(getActivity(), getString(R.string.invalid_email), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        mOnClickListener.onSubmitClick(StandardDialog.this, "");
                    }
                }
                break;
            default:
                break;
        }
    }

    public interface OnDialogClickListener {
        void onCancelClick(StandardDialog dialog);
        void onSubmitClick(StandardDialog dialog, String email);
    }
}
