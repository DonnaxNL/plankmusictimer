package com.dxapps.plankmusictimer.layouts;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.dxapps.plankmusictimer.R;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.PlayerConstants;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerCallback;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.utils.YouTubePlayerTracker;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.ui.utils.TimeUtilities;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.ui.views.YouTubePlayerSeekBar;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.ui.views.YouTubePlayerSeekBarListener;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StartTimeDialog extends DialogFragment implements View.OnClickListener, YouTubePlayerCallback{

    private static final String EXTRA_VIDEO_ID = "video_id";
    private OnStartTimeDialogClickListener mOnClickListener;

    @BindView(R.id.youtube_player_view) YouTubePlayerView mYouTubePlayer;
    @BindView(R.id.youtube_player_seekbar) YouTubePlayerSeekBar mSeekBar;
    @BindView(R.id.selected_time_text) TextView mTimeText;
    @BindView(R.id.cancel_button) Button mCancelButton;
    @BindView(R.id.submit_button) Button mSubmitButton;

    private YouTubePlayer mPlayer;
    private String mVideoId;
    private int mTempStartPosition = 0;

    public static StartTimeDialog getInstance(String videoId) {
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_VIDEO_ID, videoId);

        StartTimeDialog fragment = new StartTimeDialog();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            mVideoId = bundle.getString(EXTRA_VIDEO_ID, "");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Objects.requireNonNull(getDialog().getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.error_message_background);
        View view = inflater.inflate(R.layout.select_time_dialog, container, false);
        ButterKnife.bind(this, view);

        mYouTubePlayer.getYouTubePlayerWhenReady(this);

        return view;
    }


    @Override
    public void show(FragmentManager manager, String tag) {
        FragmentTransaction ft = manager.beginTransaction();
        ft.add(this, tag);
        ft.commitAllowingStateLoss();
    }

    @Override
    public void onYouTubePlayer(YouTubePlayer youTubePlayer) {
        mPlayer = youTubePlayer;
        mPlayer.cueVideo(mVideoId, 0);
        mSeekBar.setYoutubePlayerSeekBarListener(time -> {
            mPlayer.seekTo(time);
            mTempStartPosition = (int) time * 1000;
            mTimeText.setText(TimeUtilities.formatTime(time));
        });
        mPlayer.addListener(mSeekBar);
        mPlayer.addListener(new AbstractYouTubePlayerListener() {
            @Override
            public void onCurrentSecond(YouTubePlayer youTubePlayer, float second) {
                super.onCurrentSecond(youTubePlayer, second);
                mTempStartPosition = (int) (second * 1000);
                mTimeText.setText(TimeUtilities.formatTime(second));
            }
        });
    }

    public void setStartTimeDialogClickListener(OnStartTimeDialogClickListener listener) {
        mOnClickListener = listener;
    }

    @OnClick({R.id.cancel_button, R.id.submit_button})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancel_button:
                if (mOnClickListener != null) {
                    mYouTubePlayer.release();
                    mOnClickListener.onCancelClick(StartTimeDialog.this);
                }
                break;
            case R.id.submit_button:
                if (mOnClickListener != null) {
                    mOnClickListener.onSubmitClick(StartTimeDialog.this, mTempStartPosition);
                }
                break;
            default:
                break;
        }
    }

    public interface OnStartTimeDialogClickListener {
        void onCancelClick(StartTimeDialog dialog);
        void onSubmitClick(StartTimeDialog dialog, int startPosition);
    }
}
