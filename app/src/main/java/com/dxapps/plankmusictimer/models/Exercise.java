package com.dxapps.plankmusictimer.models;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Exercise implements Serializable {

    private String id;
    private String name;
    private String videoid;
    private long starttime;
    private long duration;

    public Exercise(String id, String name, String videoUrl, long startTime, long duration) {
        this.id = id;
        this.name = name;
        this.videoid = videoUrl;
        this.starttime = startTime;
        this.duration = duration;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVideoId() {
        return videoid;
    }

    public void setVideoId(String videoId) {
        this.videoid = videoId;
    }

    public long getStartTime() {
        return starttime;
    }

    public String getStartTimeString() {
        return msToString(starttime);
    }

    public void setStartTime(long startTime) {
        this.starttime = startTime;
    }

    public long getDuration() {
        return duration;
    }

    public String getDurationString() {
        return msToString(duration);
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    private String msToString(long milliseconds) {
        SimpleDateFormat formatter = new SimpleDateFormat("mm:ss", Locale.getDefault());
        return formatter.format(new Date(milliseconds));
    }
}
