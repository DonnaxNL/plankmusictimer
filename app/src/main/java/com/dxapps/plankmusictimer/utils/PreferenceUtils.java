package com.dxapps.plankmusictimer.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.dxapps.plankmusictimer.MainApp;
import com.dxapps.plankmusictimer.constants.SortMethod;
import com.dxapps.plankmusictimer.models.Quote;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

import java.lang.reflect.Type;
import java.security.ProtectionDomain;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PreferenceUtils {

    private static final String KEY = "sharedPreferences";
    private static final String KEY_IS_FIRST_RUN = "didFirstRun";
    private static final String KEY_IS_REGISTERED = "registered";
    private static final String KEY_COUNTDOWN_DURATION = "startPosition";
    private static final String KEY_SORT_ID = "sort_id";
    private static final String KEY_SORT_METHOD = "sort_method";
    private static final String KEY_SORT_DIRECTION = "sort_direction";
    private static final String KEY_IS_ANONYMOUS = "anonymousAccount";
    private static final String KEY_QUOTES = "quotes";

    private static PreferenceUtils sInstance = new PreferenceUtils();

    private SharedPreferences mSharedPreferences;
    private Gson gson;

    private PreferenceUtils() {
        mSharedPreferences = MainApp.getContext().getSharedPreferences(KEY, Context.MODE_PRIVATE);
        gson = new Gson();
    }

    public static synchronized PreferenceUtils getInstance() {
        return sInstance;
    }

    /**
     * Set did first run.
     */
    public void setFirstRun(Boolean signin) {
        mSharedPreferences.edit()
                .putBoolean(KEY_IS_FIRST_RUN, signin)
                .apply();
    }

    public boolean isFirstRun() {
        return mSharedPreferences.getBoolean(KEY_IS_FIRST_RUN, true);
    }

    public void setHasSignIn(Boolean signin) {
        mSharedPreferences.edit()
                .putBoolean(KEY_IS_REGISTERED, signin)
                .apply();
    }

    public boolean isRegistered() {
        return mSharedPreferences.getBoolean(KEY_IS_REGISTERED, false);
    }

    public int getCountdownDuration() {
        return mSharedPreferences.getInt(KEY_COUNTDOWN_DURATION, 5000);
    }

    public void setCountdownDuration(int duration) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(KEY_COUNTDOWN_DURATION, duration);
        editor.apply();
    }

    public int getSortId() {
        return mSharedPreferences.getInt(KEY_SORT_ID, 0);
    }

    public void setSortId(int id) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(KEY_SORT_ID, id);
        editor.apply();
    }

    public String getSortBy() {
        return mSharedPreferences.getString(KEY_SORT_METHOD, SortMethod.SORT_LAST_USED);
    }

    public void setSortBy(String sort) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(KEY_SORT_METHOD, sort);
        editor.apply();
    }

    public String getSortDirection() {
        return mSharedPreferences.getString(KEY_SORT_DIRECTION, SortMethod.SORT_DIRECTION_DESC);
    }

    public void setSortDirection(String sort) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(KEY_SORT_DIRECTION, sort);
        editor.apply();
    }

    public boolean isAnonymous() {
        return mSharedPreferences.getBoolean(KEY_IS_ANONYMOUS, false);
    }

    public void setAnonymous(boolean value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(KEY_IS_ANONYMOUS, value);
        editor.apply();
    }

    public List<Quote> getQuotes() {
        Type type = new TypeToken<List<Quote>>(){}.getType();
        String quotes = mSharedPreferences.getString(KEY_QUOTES, "");
        return gson.fromJson(quotes, type);
    }

    public void setQuotes(List<Quote> list) {
        String quotes = gson.toJson(list);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(KEY_QUOTES, quotes);
        editor.apply();
    }

    public void resetPreferences() {
        setHasSignIn(false);
        setAnonymous(false);
        setCountdownDuration(5000);
        setSortId(0);
        setSortBy(SortMethod.SORT_LAST_USED);
        setSortDirection(SortMethod.SORT_DIRECTION_DESC);
        setQuotes(null);
    }
}
